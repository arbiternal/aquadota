var gulp = require('gulp'),
    usemin = require('gulp-usemin'),
    wrap = require('gulp-wrap'),
    connect = require('gulp-connect'),
    watch = require('gulp-watch'),
    minifyCss = require('gulp-cssnano'),
    minifyJs = require('gulp-uglify'),
    concat = require('gulp-concat'),
    less = require('gulp-less'),
    rename = require('gulp-rename'),
    minifyHTML = require('gulp-htmlmin');

var paths = {
    scripts: 'src/js/**/*.*',
    less: 'src/less/**/*.*',
    style: 'src/css/**/*.*',
    images: 'src/img/**/*.*',
    pages: 'src/pages/**/*.html',
    controllers: 'src/pages/**/*.js',
    css: 'src/pages/**/*.css',
    index: 'src/index.html',
    bower_fonts: 'src/components/**/*.{ttf,woff,woff2,eof,svg}',
    files: "src/files/*.*"
};

/**
 * Handle bower components from index
 */
gulp.task('usemin', function() {
    return gulp.src(paths.index)
        .pipe(usemin({
            js: [minifyJs(), 'concat'],
            css: [minifyCss({keepSpecialComments: 0}), 'concat'],
        }))
        .pipe(gulp.dest('dist/'));
});

/**
 * Copy assets
 */
gulp.task('build-assets', ['copy-bower_fonts']);

gulp.task('copy-bower_fonts', function() {
    return gulp.src(paths.bower_fonts)
        .pipe(rename({
            dirname: '/fonts'
        }))
        .pipe(gulp.dest('dist/lib'));
});

/**
 * Handle custom files
 */
gulp.task('build-custom', ['custom-images', 'custom-js', /*'custom-less',*/ 'custom-css', 'custom-pages', 'server-files']);

gulp.task('custom-images', function() {
    return gulp.src(paths.images)
        .pipe(gulp.dest('dist/img'));
});

gulp.task('custom-js', function() {
    return gulp.src([paths.scripts, paths.controllers])
        //.pipe(minifyJs())
        .pipe(concat('dashboard.min.js'))
        .pipe(gulp.dest('dist/js'));
});

//gulp.task('custom-less', function() {
//    return gulp.src(paths.less)
//        .pipe(less())
//        .pipe(gulp.dest('dist/css'));
//});

gulp.task('custom-css', function() {
    return gulp.src([paths.style, paths.css])
        .pipe(minifyCss())
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('custom-pages', function() {
    return gulp.src(paths.pages)
        .pipe(minifyHTML())
        .pipe(gulp.dest('dist/pages'));
});

gulp.task('server-files', function() {
    return gulp.src(paths.files)
        .pipe(gulp.dest('dist/files'));
});

/**
 * Watch custom files
 */
gulp.task('watch', function() {
    gulp.watch([paths.images], ['custom-images']);
    /*gulp.watch([paths.less], ['custom-less']);*/
    gulp.watch([paths.scripts, paths.controllers], ['custom-js']);
    gulp.watch([paths.pages], ['custom-pages']);
    gulp.watch([paths.css, paths.style], ['custom-css']);
    gulp.watch([paths.index], ['usemin']);
    gulp.watch([paths.files], ['server-files']);
});

/**
 * Live reload server
 */
gulp.task('webserver', function() {
    connect.server({
        root: 'dist',
        livereload: true,
        port: 8888
    });
});

gulp.task('livereload', function() {
    /*watches everything but the html*/
    gulp.src(['dist/**/*.*'])
        .pipe(watch(['dist/**/*.*']))
        .pipe(connect.reload());
    /*watches the pages htmls*/
    gulp.src(['dist/pages/**/*.*'])
        .pipe(watch(['dist/pages/**/*.*']))
        .pipe(connect.reload());
});

/**
 * Gulp tasks
 */
gulp.task('build', ['usemin', 'build-assets', 'build-custom']);
gulp.task('default', ['build', 'webserver', 'livereload', 'watch']);