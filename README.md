# KICT
## Based on rdash angular by Elliot Hesp
## https://github.com/rdash/rdash-angular

This is a project for vis.js proof of concept testing.

## Credits
* [Elliot Hesp](https://github.com/Ehesp)
* [Leonel Samayoa](https://github.com/lsamayoa)
* [Mathew Goldsborough](https://github.com/mgoldsborough)
* [Ricardo Pascua Jr](https://github.com/rdpascua)