/**
 * Master Controller
 */

var templateCtrl = function ($scope, $cookies, auth) {
    /**
     * Sidebar Toggle & Cookie Control
     */
    var mobileView = 992;

    $scope.getWidth = function () {
        return window.innerWidth;
    };
    
    var toggleFunction = function (newValue, oldValue) {
        if (newValue >= mobileView) {
            $scope.toggle = !auth.getToggle();
        } else {
            $scope.toggle = false;
        }
        
        $scope.toggle = true;
        return newValue;
    };

    $scope.$watch($scope.getWidth, toggleFunction);

    $scope.toggleSidebar = function () {
        $scope.toggle = !$scope.toggle;
        auth.setToggle($scope.toggle);
    };

    /*on window resize, reinit scope (for toggle functions)*/
    window.onresize = function () {
        $scope.$apply();
    };
    
    $scope.logout = auth.logout;
    
    $scope.init = function (){
        $scope.currentpage = auth.checkUser();
        $scope.user = auth.getUser();
        $scope.user.session = auth.session;
        return $scope.user;
    };
    
    return $scope.init();
};

app.controller("templateCtrl", ["$scope", "$cookies", "auth", templateCtrl]);