/**
 * dashboard Controller
 */

var networkAllCtrl = function ($scope, $http, $timeout) {
    var options = {
        //autoResize: true,
        //height: '100%',
        //width: '100%',
        //locale: 'en',
        //locales: "locales",
        //clickToUse: false,
        //configure: {...
        //}, // defined in the configure module.
        edges: {
            physics: false,
            smooth: false
        },
        nodes: {
            shape: "circle",
            size: 50,
            widthConstraint: {
                minimum: 50,
                maximum: 50
            }
        },
        groups: {
            employess: {
                shape: "circle",
            },
            location: {
                shape: "box",
                color: {
                    background: "#984211",
                    border: "#984211",
                },
                font: {
                    color: "white"
                },
                margin: 15
            }
        }, // defined in the groups module.
        layout: {
            improvedLayout: false
        }, // defined in the layout module.
        //interaction: {...
        //}, // defined in the interaction module.
        //manipulation: {...
        //}, // defined in the manipulation module.
        //physics: {...
        //} // defined in the physics module.
    };

    var addIncrement = function(dataSet, objArray, count, time){
        if (!objArray.constructor === Array) objArray = [objArray];
        count = objArray.length < 10 ? objArray.length : count || 10;
        time = time || 100;
        
        var arrayToInject = objArray.slice(0, count);
        
        $timeout(function(){
            dataSet.add(arrayToInject);
        
            if (objArray.length <= count){
                return true;
            } else {
                addIncrement(dataSet, objArray.slice(count, objArray.length), count, time);
            }
        }, time);
    };
    
    $scope.getData = function (dataSetName) {
        $http.get("./files/data.json").then(function (res) {
            $scope.jsonData = res.data;

            /*these will be sorted node data*/
            $scope.employees = [];
            $scope.techniques = [];

            /*these will be sorted link data*/
            $scope.employeeLinks = [];
            $scope.techniqueLinks = [];

            /*this is the sorted raw technic data*/
            $scope.technic = {
                first: {},
                second: {},
                third: {}
            };

            angular.forEach(res.data, function (employeeValue, employeeKey) {
                var employee = {
                    _id: employeeValue._id,
                    id: employeeValue.username, //using username as node id
                    administrator: employeeValue.administrator,
                    label: employeeValue.name, //using name as label
                    patent: employeeValue.patent,
                    thesis: employeeValue.thesis,
                    fund: employeeValue.fund,
                    phone: employeeValue.phone,
                    email: employeeValue.email,
                    position: employeeValue.position,
                    department: employeeValue.department,
                    technic: []
                };

                angular.forEach(employeeValue.technics, function (technicValue, technicKey) {
                    /*대*/
                    $scope.technic.first[technicValue.first.cdBdn] = technicValue.first;
                    /*중*/
                    $scope.technic.second[technicValue.second.cdBdn] = technicValue.second;
                    /*소*/
                    $scope.technic.third[technicValue.third.cdBdn] = technicValue.third;
                    /*detail은 소 중에도 불류 되지만 쉽게 하기 위해 그냥 한다*/
                    $scope.technic.third[technicValue.third.cdBdn]["detail"] = technicValue.detail.cdBdnNm;

                    var technic = {
                        first: technicValue.first.cdBdn,
                        second: technicValue.second.cdBdn,
                        third: technicValue.third.cdBdn
                    };

                    employee.technic.push(technic);
                });

                $scope.employees.push(employee);
            });
        });
    };

    /*generates a dataset for the large pool*/
    $scope.generateFirstData = function () {
        $scope.technicsFirst = [];
        angular.forEach($scope.technic.first, function (firstVal, firstKey) {
            var technique = {
                id: firstVal.cdBdn,
                label: firstVal.cdBdnNm,
                value: 30,
                color: "#111b99"
            };
            $scope.techniques.push(technique);
            $scope.technicsFirst.push(technique);
        });
        $scope.data.nodes.add($scope.technicsFirst);
    };

    /*generates a dataset for the medium pool*/
    $scope.generateSecondData = function (firstDataSetName) {
        $scope.technicsSecond = [];
        $scope.technicsSecondLinks = [];
        angular.forEach($scope.technic.second, function (secondVal, secondKey) {
            var technique = {
                id: secondVal.cdBdn,
                label: secondVal.cdBdnNm,
                value: 20,
                color: "#114599"
            };
            $scope.techniques.push(technique);
            $scope.technicsSecond.push(technique);
            /*links second med details to large details*/
            var secondLink = {
                from: secondVal.cdBdn,
                to: secondVal.uppCdBdn
            };
            $scope.techniqueLinks.push(secondLink);
            $scope.technicsSecondLinks.push(secondLink);
        });
        $scope.data.nodes.add($scope.technicsSecond);
        $scope.data.edges.add($scope.technicsSecondLinks);
    };

    /*generates a dataset for the small pool*/
    $scope.generateThirdData = function (secondDataSetName) {
        $scope.technicsThird = [];
        $scope.technicsThirdLinks = [];
        angular.forEach($scope.technic.third, function (thirdVal, thirdKey) {
            var technique = {
                id: thirdVal.cdBdn,
                label: thirdVal.cdBdnNm,
                value: 10,
                color: "#116f99"
            };
            $scope.techniques.push(technique);
            $scope.technicsThird.push(technique);
            /*links third small details to med details*/
            var thirdLink = {
                from: thirdVal.cdBdn,
                to: thirdVal.uppCdBdn
            };
            $scope.techniqueLinks.push(thirdLink)
            $scope.technicsThirdLinks.push(thirdLink);
        });
        $scope.data.nodes.add($scope.technicsThird);
        $scope.data.edges.add($scope.technicsThirdLinks);
    };

    /*links employees to techniques*/
    $scope.generateEmployeeData = function () {
        //var employeePromise = $timeout();
        
        angular.forEach($scope.employees, function (employeeVal, employeeKey) {
            //employeePromise = employeePromise.then(function(){
                angular.forEach(employeeVal.technic, function (technicVal, technicKey) {
                    var firstLink = {
                        from: employeeVal.id,
                        to: technicVal.first,
                        type: "first"
                    };
                    $scope.employeeLinks.push(firstLink);
                    var secondLink = {
                        from: employeeVal.id,
                        to: technicVal.second,
                        type: "second"
                    };
                    $scope.employeeLinks.push(secondLink);
                    var thirdLink = {
                        from: employeeVal.id,
                        to: technicVal.third,
                        type: "third"
                    };
                    $scope.employeeLinks.push(thirdLink);
                    
                    /*$scope.data.edges.add(firstLink);
                    $scope.data.edges.add(secondLink);
                    $scope.data.edges.add(thirdLink);*/
                });
                
                /*$scope.data.nodes.add(employeeVal);*/
                
                //return $timeout(100);
            //});
        });

        addIncrement($scope.data.nodes, $scope.employees, $scope.employees/50);
        addIncrement($scope.data.edges, $scope.employeeLinks, $scope.employeeLinks/50);
    };
    
    $scope.generateDepartments = function(){
        $scope.deptNodeList = {};
        angular.forEach($scope.employees, function (employeeVal, employeeKey) {
            var deptNode = {
                id: employeeVal.department.code,
                label: employeeVal.department.name,
                group: "location"
            }
            $scope.deptNodeList[employeeVal.department.code] = deptNode;
        });
    };

    /*links employees to departments*/
    $scope.generateDepartmentData = function () {
        $scope.deptNodes = [];
        $scope.deptNodeList = {};
        $scope.deptLinks = [];
        angular.forEach($scope.employees, function (employeeVal, employeeKey) {
            var deptNode = {
                id: employeeVal.department.code,
                label: employeeVal.department.name,
                group: "location"
            }
            $scope.deptNodeList[employeeVal.department.code] = deptNode;
            var deptLink = {
                from: employeeVal.id,
                to: employeeVal.department.code
            };
            $scope.deptLinks.push(deptLink);
        });
        
        angular.forEach($scope.deptNodeList, function(deptVal, deptKey){
            $scope.deptNodes.push(deptVal);
        });
        
        $scope.data.nodes.add($scope.deptNodes);
        $scope.data.edges.add($scope.deptLinks);
    };

    $scope.init = function () {
        /*reset data*/
        $scope.data = {
            nodes: new vis.DataSet([]),
            edges: new vis.DataSet([])
        };
        /*fetch data*/
        $scope.getData();
        /*make a network graph from an empty dataset (injection will happen later)*/
        $scope.networkChart = new vis.Network(document.getElementById('network-all-container'), $scope.data, options);
    };

    $scope.init();
};

angular.module("KICT").controller("networkAllCtrl", ["$scope", "$http", "$timeout", networkAllCtrl]);