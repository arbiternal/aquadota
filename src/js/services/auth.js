/**
 * User Auth checking service
 */

var authService = function ($cookies, $state) {
    this.session = true;
    /*check for all local preferences*/
    this.checkLocalPreferences = function () {
        /*assign a default preference*/
        var defaultPreference = {
            remember: true
        };
        /*attempt to get local preferences, if none exist, overwrite*/
        return this.getUser() || defaultPreference;
    };
    /*fetch the saved user info*/
    this.getUser = function () {
        var defaultUserInfo = {
            session: false
        };
        return $cookies.getObject("user") || defaultUserInfo;
    };
    /*check if the user is valid (NYI)*/
    this.checkUser = function (user) {
        return user.name && user.password;
    };
    /*save a user defined user object*/
    this.setUser = function (user) {
        return $cookies.putObject("user", user);
    };
    /*log in*/
    this.login = function (user) {
        if (!user) user = this.getUser();
        /*if the user validation fails, return an error message*/
        if (!this.checkUser(user)) return alert("Failed to log in");
        /*turn on session*/
        this.session = true;
        /*if the login is successful, save the user data and move to a local page*/
        this.setUser(user);
        /*reload page if it's not login page, otherwise move to dashboards*/
        var targetLocation = $state.current.name == "template.login" ? "template.networkAll" : $state.current.name;
        var reloadOption = {
            reload: true
        };
        return $state.go(targetLocation, {}, reloadOption);
    };
    /*log out*/
    this.logout = function () {
        /*turn off session*/
        this.session = false;
        /*go to login page*/
        var reloadOption = {
            reload: true
        };
        $state.go("template.login", {}, reloadOption);
    };
    /*check if the user session is valid*/
    this.checkUser = function () {
        var currentpage = {};
        if ($state.current.name !== 'template.login' && !this.session) {
            this.logout();
        } else {
            /*the currentpage.title should be a switch case*/
            currentpage.title = $state.current.name.slice(9);
            currentpage.breadcrumbs = "";
        }
        return currentpage;
    };
    /*get toggle settings*/
    this.getToggle = function () {
        return $cookies.get("toggle") ? true : false;
    };
    /*set toggle settings*/
    this.setToggle = function (toggle) {
            return $cookies.put("toggle", toggle);
        }
        /*return the service object*/
    return this;
};

angular.module("KICT").service("auth", ["$cookies", "$state", authService]);